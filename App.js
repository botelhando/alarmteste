/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react'
import {Platform, StyleSheet, Text, View, Button, DeviceEventEmitter} from 'react-native'
import ReactNativeAN from 'react-native-alarm-notification'

const configAlarm = {
  id: 'wakeup',
  title: 'One Minute...',
  message: 'Alarme de dez segundos acionado',
  channel: "wakeup",
  ticker: "My Notification Ticker",
  vibrate: true,
  mall_icon: "ic_launcher",                    
  large_icon: "ic_launcher",
  play_sound: true,
  sound_name: 'teste.mp3',                             // Plays custom notification ringtone if sound_name: null
  schedule_once: true,
  auto_cancel: true
  
}

export default class App extends Component {
  constructor(props, context) {
		super(props, context);
		this.state = {
      alarme: 0
    }
		this._scheduleAlarm = this._scheduleAlarm.bind(this);
		this._stopAlarm = this._stopAlarm.bind(this);
	}
  


  _scheduleAlarm = () => {
    const clockAlarm = ReactNativeAN.parseDate(new Date(Date.now() + 30000))
    this.setState({alarme: clockAlarm})
    const config = {...configAlarm, fire_date: clockAlarm, }


    ReactNativeAN.scheduleAlarm(config)
    //ReactNativeAN.sendNotification(config)
  }

  _stopAlarm = () => {
    ReactNativeAN.stopAlarm()
  }


  componentDidMount(){
    const clockAlarm = ReactNativeAN.parseDate(new Date(new Date().setMinutes(4)) )
    this.setState({alarme: clockAlarm})
    const config = {...configAlarm, fire_date: clockAlarm }
    
    ReactNativeAN.scheduleAlarm(config)
    //ReactNativeAN.sendNotification(config)
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Alarm Manager Teste {this.state.alarme}</Text>
        <Button title='Criar Alarme de 10 segundos' style={{margin:20}} onPress={() => {this._scheduleAlarm()}} />
        <Button title='Parar o alarme' onPress={() => {this._stopAlarm()}} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
